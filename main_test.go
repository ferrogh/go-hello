package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHello(t *testing.T) {
	req, err := http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatal(err)
	}

	recorder := httptest.NewRecorder()
	handler := http.HandlerFunc(Hello)
	handler.ServeHTTP(recorder, req)

	if recorder.Code != http.StatusOK {
		t.Errorf("Handler returned %v expected %v", recorder.Code, http.StatusOK)
	}

	expected := "Hello World"
	if recorder.Body.String() != expected {
		t.Errorf("Handler returned unexpected body: \n\t%+v\nExpected: \n\t%+v",
			recorder.Body.String(), expected)
	}
}
